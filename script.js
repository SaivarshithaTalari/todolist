function addTodo() {
      const todoInput = document.getElementById("todoInput");
      const todoList = document.getElementById("todoList");

      if (todoInput.value !== "") {
            const newTodo = document.createElement("li");
            const deleteBtn = document.createElement("span");
            newTodo.innerText = todoInput.value;
            deleteBtn.innerText = " ❌ ";
            deleteBtn.className = "delete-btn";
            deleteBtn.onclick = function () {
                  newTodo.remove();
            };

            newTodo.appendChild(deleteBtn);
            todoList.appendChild(newTodo);
            todoInput.value = "";

            newTodo.onclick = function () {
                  editTask(newTodo);
            };
      } else {
            alert("Please enter a task!");
      }
}

function editTask(task) {
      const updatedTask = prompt("Edit task:", task.firstChild.textContent.trim());
      if (updatedTask !== null) {
            task.firstChild.textContent = updatedTask;
      }
}

function searchTodo() {
      const searchInput = document.getElementById("searchInput").value.toLowerCase();
      const todos = document.getElementsByTagName("li");

      for (let i = 0; i < todos.length; i++) {
            const todoText = todos[i].textContent.toLowerCase();
            if (todoText.indexOf(searchInput) > -1) {
                  todos[i].style.display = "";
            } else {
                  todos[i].style.display = "none";
            }
      }
}
